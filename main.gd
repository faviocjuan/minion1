
extends Node2D

var pantalla
var bask_size
const BASK_SPEED = 450
var direction
var garb_speed
var garb_size
onready var garbage = get_node("spr_garbage")
var puntos


func _ready():
	pantalla = get_viewport_rect().size
	bask_size = get_node("spr_basket").get_texture().get_size()
	garb_size = get_node("spr_garbage").get_texture().get_size()
	garb_speed = 250
	direction = Vector2(0,1)
	puntos = 0
	set_process(true)
	
	
	
	
func _process(delta):
	
	var garb_pos = get_node("spr_garbage").get_pos()
	var bask_pos = get_node("spr_basket").get_pos()
	var rect_bask = Rect2(get_node("spr_basket").get_pos() - bask_size / 2, bask_size)
	var rect_garb = Rect2(get_node("spr_garbage").get_pos() - bask_size / 2, bask_size)
	garb_pos += direction * garb_speed * delta
	
	if(rect_bask.intersects(rect_garb)):
		if(garb_pos.x > bask_pos.x + bask_size.x/2):
			garb_speed = 550 
			direction = Vector2(rand_range(0.3,3), rand_range(0.5,1)) 
		if(garb_pos.x < bask_pos.x - bask_size.x/2):
			garb_speed = 550 
			direction = Vector2(rand_range(0.3,-3), rand_range(0.5,1)) 
		if(garb_pos.x < bask_pos.x + bask_size.x/2 and garb_pos.x > bask_pos.x - bask_size.x/2):
			puntos += 1
			#garbage.queue_free()
	get_node("spr_garbage").set_pos(garb_pos) # Cambiar de posicion al final de los if()
	
	
	if(bask_pos.x > 0 and Input.is_action_pressed("ui_left")):
		bask_pos += Vector2(-1,0) * BASK_SPEED * delta
	if(bask_pos.x < pantalla.x -  bask_size.x/2 and Input.is_action_pressed("ui_right")):
		bask_pos += Vector2(1,0) * BASK_SPEED * delta
	get_node("spr_basket").set_pos(bask_pos)



func _on_Timer_timeout():
		var p = get_node("spr_garbage").set_pos(Vector2(rand_range(0, 1024),0))
		direction = Vector2(0,1)
		print(puntos)